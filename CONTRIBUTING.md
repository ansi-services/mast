# Contribute

To contribute please contact first with company owner at mail [marcin@karkocha.tech](mailto://marcin@karkocha.tech)

## Legal

Software delivery and feature requestion process is totally under control of On Technologies Marcin Karkocha company registered in Gdańsk, Poland.

## Community

You can also join our slack here: [Slack Ansi.Services](https://join.slack.com/t/ansiservices/shared_invite/zt-1b4ysogtz-kH3sy7M_JVKWU67JRptYcw)
