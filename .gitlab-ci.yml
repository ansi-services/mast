image: python:3.7-slim

stages:
  - prepare
  - test
  - build
  - release

variables:
  CI: "true"
  KIND_VERSION: v0.11.1
  KUBECTL_VERSION: v1.22.4
  PYTHON_VERSION: 3.7.0
  PYENV_HOME: /root/.pyenv
  PACKAGE_REGISTRY_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/mast/${CI_COMMIT_TAG}"
  DARWIN_AMD64_BINARY: mast-darwin-amd64
  LINUX_AMD64_BINARY: mast-linux-amd64
  LINUX_ARM64_BINARY: mast-linux-arm64
  LINUX_PPC64LE_BINARY: mast-linux-ppc64le
  LINUX_S390X_BINARY: mast-linux-s390x
  WINDOWS_AMD64_BINARY: mast-windows-amd64

build mast cli:
  stage: prepare
  image: golang:1.18.3-bullseye
  script:
    - cd cli
    - go build
    - chmod +x mast
  artifacts:
    expire_in: 1 week
    paths:
      - cli/mast
  cache:
    paths:
      - $HOME/go/
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_TAG && $CI_COMMIT_BRANCH == "master"
    - if: $CI_COMMIT_TAG

build test image:
  stage: prepare
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - mkdir -p /kaniko/.docker
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor --context "${CI_PROJECT_DIR}/ci" --build-arg "PYTHON_VERSION=${PYTHON_VERSION}" --build-arg "KIND_VERSION=${KIND_VERSION}" --build-arg "KUBECTL_VERSION=${KUBECTL_VERSION}" --build-arg "PYENV_HOME='${PYENV_HOME}'" --dockerfile "${CI_PROJECT_DIR}/ci/Dockerfile.e2e" --destination "${CI_REGISTRY_IMAGE}/mast-e2e:${CI_COMMIT_TAG}" --destination "${CI_REGISTRY_IMAGE}/mast-e2e:latest"
  rules:
    - if: $CI_COMMIT_BRANCH == "master" 
      changes:
        - .gitlab-ci.yml
        - ci/Dockefile.e2e
      when: always

lint:
  stage: prepare
  before_script:
    # creating pipenv
    - cd controller
    - pip install pipenv
    - pipenv install --dev
  script:
    - pipenv run black .
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_TAG

unit tests:
  stage: test
  before_script:
    - cd controller
    - pip install pipenv
    - pipenv install --dev
  script:
    - pipenv run python -m pytest tests/unit_tests -s --failed-first -x
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_TAG

e2e tests:
  variables:
    DOCKER_HOST: tcp://docker:2375
    DOCKER_TLS_CERTDIR: ""
  needs: [build mast cli]
  stage: test
  services:
    - docker:20.10.17-dind
  image: ${CI_REGISTRY_IMAGE}/mast-e2e:latest
  before_script:
    - kind create cluster --config=./controller/tests/test_manifests/kind-config.yaml
    - sed -i -E -e 's/localhost|0\.0\.0\.0/docker/g' "$HOME/.kube/config"
    # creating pipenv
    - cd controller
    - export PATH=${PYENV_HOME}/shims:${PYENV_HOME}/bin:$PATH
    - eval "$(pyenv init -)"
    - pipenv install --dev
    - export $CI
    - echo "$TEST_SECRET_GIT_BASIC_AUTH" > "tests/test_manifests/test-secret-git-basic-auth.yaml"
    - echo "$TEST_SECRET_GIT_SSH_AUTH" > "tests/test_manifests/test-secret-git-ssh-auth.yaml"
  script:
    - pipenv run python -m pytest tests/e2e_tests/ -x -vv -l --tb=long
  cache:
    key: "e2e test binaries"
    paths:
      - usr/local/bin/kubectl
      - usr/local/bin/kind
      - $PYENV_HOME/
      - ${CI_PROJECT_DIR}/controller/tests/__pycache__/
      - ${CI_PROJECT_DIR}/controller/tests/.pytest_cache
      - .cache/pipenv
      - .cache/pip
      - .pyenv
    when: "always"
    untracked: true
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_TAG && $CI_COMMIT_BRANCH == "master"

build mast image:
  stage: build
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - mkdir -p /kaniko/.docker
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor --context "${CI_PROJECT_DIR}/controller" --dockerfile "${CI_PROJECT_DIR}/controller/Dockerfile" --destination "${CI_REGISTRY_IMAGE}/mast:${CI_COMMIT_TAG}" --destination "${CI_REGISTRY_IMAGE}/mast:latest"
  rules:
    - if: $CI_COMMIT_TAG

generate manifest:
  stage: release
  image:
    name: k8s.gcr.io/kustomize/kustomize:v4.1.0
    entrypoint: [""]
  script:
    - |
      cd controller/manifests/overlays/prod
      kustomize edit set image mast-operator-image=registry.gitlab.com/ansi-services/mast/mast:${CI_COMMIT_TAG}
      kustomize build > ${CI_PROJECT_DIR}/mast-installer.yaml
  artifacts:
    paths:
      - mast-installer.yaml
  rules:
    - if: $CI_COMMIT_TAG

build mast cli binaries:
  stage: release
  image: golang:1.18.3-bullseye
  script:
    - cd cli
    # change version in cli
    - >
      sed -i -e 's/Version: ......./Version: "'${CI_COMMIT_TAG}'"/g' cmd/root.go
    - GOOS=darwin GOARCH=amd64 go build -o bin/${DARWIN_AMD64_BINARY}
    - GOOS=linux GOARCH=amd64 go build -o bin/${LINUX_AMD64_BINARY}
    - GOOS=linux GOARCH=arm64 go build -o bin/${LINUX_ARM64_BINARY}
    - GOOS=linux GOARCH=ppc64le go build  -o bin/${LINUX_PPC64LE_BINARY}
    - GOOS=linux GOARCH=s390x go build -o bin/${LINUX_S390X_BINARY}
    - GOOS=windows GOARCH=amd64 go build -o bin/${WINDOWS_AMD64_BINARY}
  artifacts:
    paths:
      - cli/bin/
  rules:
    - if: $CI_COMMIT_TAG

create package:
  needs: ["generate manifest", "build mast cli binaries"]
  stage: release
  image: curlimages/curl:latest
  script:
    - 'curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file mast-installer.yaml "${PACKAGE_REGISTRY_URL}/mast-installer.yaml"'
    - 'curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file cli/bin/${DARWIN_AMD64_BINARY} "${PACKAGE_REGISTRY_URL}/${DARWIN_AMD64_BINARY}"'
    - 'curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file cli/bin/${LINUX_AMD64_BINARY} "${PACKAGE_REGISTRY_URL}/${LINUX_AMD64_BINARY}"'
    - 'curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file cli/bin/${LINUX_ARM64_BINARY} "${PACKAGE_REGISTRY_URL}/${LINUX_ARM64_BINARY}"'
    - 'curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file cli/bin/${LINUX_PPC64LE_BINARY} "${PACKAGE_REGISTRY_URL}/${LINUX_PPC64LE_BINARY}"'
    - 'curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file cli/bin/${LINUX_S390X_BINARY} "${PACKAGE_REGISTRY_URL}/${LINUX_S390X_BINARY}"'
    - 'curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file cli/bin/${WINDOWS_AMD64_BINARY} "${PACKAGE_REGISTRY_URL}/${WINDOWS_AMD64_BINARY}"'
  rules:
    - if: $CI_COMMIT_TAG

release package:
  needs: [create package]
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  script:
    - echo "Releasing package"
  release:
    name: "$CI_COMMIT_TAG"
    description: $CI_COMMIT_DESCRIPTION # $EXTRA_DESCRIPTION must be defined
    tag_name: "$CI_COMMIT_TAG" # elsewhere in the pipeline.
    ref: "$CI_COMMIT_TAG"
    assets:
      links:
        - name: mast-installer.yaml
          url: ${PACKAGE_REGISTRY_URL}/mast-installer.yaml
        - name: ${DARWIN_AMD64_BINARY}
          url: ${PACKAGE_REGISTRY_URL}/${DARWIN_AMD64_BINARY}
        - name: ${LINUX_AMD64_BINARY}
          url: ${PACKAGE_REGISTRY_URL}/${LINUX_AMD64_BINARY}
        - name: ${LINUX_ARM64_BINARY}
          url: ${PACKAGE_REGISTRY_URL}/${LINUX_ARM64_BINARY}
        - name: ${LINUX_PPC64LE_BINARY}
          url: ${PACKAGE_REGISTRY_URL}/${LINUX_PPC64LE_BINARY}
        - name: ${LINUX_S390X_BINARY}
          url: ${PACKAGE_REGISTRY_URL}/${LINUX_S390X_BINARY}
        - name: ${WINDOWS_AMD64_BINARY}
          url: ${PACKAGE_REGISTRY_URL}/${WINDOWS_AMD64_BINARY}
  rules:
    - if: $CI_COMMIT_TAG
