import subprocess
import time
import kopf.testing
import pytest
from tests.commons import (
    TEST_MANIFESTS_DIR,
    CLI_DIR,
    MAST_SERVER,
    cleanup_mastjobs,
    cleanup_mastruns,
    settings,
)

mj_obj_yaml = TEST_MANIFESTS_DIR.joinpath("mj-git-basic-auth.yaml")

mr_obj_yaml = TEST_MANIFESTS_DIR.joinpath("mr-git-basic-auth.yaml")

test_secret_basic_auth_obj_yaml = TEST_MANIFESTS_DIR.joinpath(
    "test-secret-git-basic-auth.yaml"
)


class TestCmdJob:
    """
    test for the cmd mast job
    """

    # @pytest.mark.usefixtures("prepare_cluster")
    def test_mast_job_list_with_empty_return(self):
        """mast job list - check  no jobs returned"""

        mast_cmd = subprocess.run(
            [CLI_DIR.joinpath("mast"), "job", "list"],
            shell=False,
            timeout=10,
            capture_output=True,
            text=True,

        )
        time.sleep(1)

        assert "No must jobs found" in mast_cmd.stderr

    def test_mast_job_create(self, cleanup_mastjobs):
        """mast job create - check if mastjob is created"""
        mast_cmd = subprocess.run(
            [CLI_DIR.joinpath("mast"), "job", "create", mj_obj_yaml],
            check=True,
            shell=False,
            timeout=10,
            capture_output=True,
        )
        time.sleep(1)
        assert "MastJob git-basic-auth created" in mast_cmd.stderr.decode("utf-8")

    def test_mast_job_list_with_one_job(self, cleanup_mastjobs):
        """mast job list - check one job returned"""

        subprocess.run(["kubectl", "apply", "-f", mj_obj_yaml], shell=False, timeout=10)

        time.sleep(1)
        mast_cmd = subprocess.run(
            [CLI_DIR.joinpath("mast"), "job", "list"],
            check=True,
            shell=False,
            timeout=10,
            capture_output=True,
        )

        assert "git-basic-auth" in mast_cmd.stdout.decode("utf-8")

    def test_mast_job_run(self, cleanup_mastjobs, cleanup_mastruns):
        """mast job run - check if mastrun is created and workflow triggered"""

        with kopf.testing.KopfRunner(
            ["run", "--all-namespaces", str(MAST_SERVER)],
            timeout=60,
            settings=settings,
        ) as runner:
            
            subprocess.run(
                ["kubectl", "apply", "-f", test_secret_basic_auth_obj_yaml],
                check=True,
                shell=False,
                timeout=10,
            )

            subprocess.run(
                ["kubectl", "apply", "-f", mj_obj_yaml],
                check=True,
                shell=False,
                timeout=10,
            )
            time.sleep(1)
            mast_cmd = subprocess.run(
                [CLI_DIR.joinpath("mast"), "job", "run", "git-basic-auth"],
                check=True,
                shell=False,
                timeout=10,
                capture_output=True,
            )

            assert "MastRun created, job git-basic-auth" in mast_cmd.stderr.decode(
                "utf-8"
            )

            get_mr = subprocess.run(
                ["kubectl", "get", "mr"],
                check=True,
                shell=False,
                timeout=10,
                capture_output=True,
            )

        workflow_name = subprocess.run(
                ["kubectl", "get", "wf"],
                check=True,
                shell=False,
                timeout=10,
                capture_output=True,
            )

        assert runner.exception is None
        assert runner.exit_code == 0
        assert "git-basic-auth" in get_mr.stdout.decode("utf-8")
        assert "git-basic-auth" in workflow_name.stdout.decode("utf-8")

    def test_mast_job_delete_by_name(self, cleanup_mastjobs):
        """mast job delete - check if mast job is deleted"""

        subprocess.run(
            ["kubectl", "apply", "-f", mj_obj_yaml], check=True, shell=False, timeout=10
        )

        mast_cmd = subprocess.run(
            [CLI_DIR.joinpath("mast"), "job", "delete", "git-basic-auth"],
            shell=False,
            check=True,
            timeout=10,
            capture_output=True,
        )
        time.sleep(1)
        assert "MastJob git-basic-auth deleted" in mast_cmd.stderr.decode("utf-8")

    def test_mast_job_delete_by_file(self, cleanup_mastjobs):
        """mast job delete - check if mast job is deleted"""

        subprocess.run(
            ["kubectl", "apply", "-f", mj_obj_yaml], check=True, shell=False, timeout=10
        )

        mast_cmd = subprocess.run(
            [CLI_DIR.joinpath("mast"), "job", "delete", mj_obj_yaml],
            shell=False,
            check=True,
            timeout=10,
            capture_output=True,
        )

        assert f"MastJob {mj_obj_yaml} deleted" in mast_cmd.stderr.decode("utf-8")
