import subprocess
import time
import kopf.testing
import pytest
from tests.commons import TEST_MANIFESTS_DIR, MAST_SERVER, wait_for_workflow_to_finish

"""
Test to check loading vars from lieteral
"""

mj_obj_yaml = TEST_MANIFESTS_DIR.joinpath("mj-vars-ext-repo.yaml")

mr_obj_yaml = TEST_MANIFESTS_DIR.joinpath("mr-vars-ext-repo.yaml")

playbook = TEST_MANIFESTS_DIR.joinpath("playbook-config2.yaml")


@pytest.fixture(autouse=True)
def obj_absent():
    # make sure objects dont exists so it could triiger mast server functions
    yield
    subprocess.run(
        ["kubectl", "delete", "-f", mr_obj_yaml], check=True, timeout=10, shell=False
    )


def test_local_run():
    # To prevent lengthy threads in the loop executor when the process exits.
    settings = kopf.OperatorSettings()
    settings.watching.server_timeout = 10
    # Run an operator and simulate some activity with the operated resource.
    with kopf.testing.KopfRunner(
        ["run", "--all-namespaces", str(MAST_SERVER)],
        timeout=60,
        settings=settings,
    ) as runner:
        subprocess.run(
            ["kubectl", "apply", "-f", mj_obj_yaml],
            shell=False,
            timeout=10,
            check=True,
        )
        time.sleep(1)  # give it some time to react
        subprocess.run(
            ["kubectl", "apply", "-f", playbook], shell=False, timeout=10, check=True
        )
        time.sleep(1)  # give it some time to react
        subprocess.run(
            ["kubectl", "apply", "-f", mr_obj_yaml], shell=False, check=True, timeout=10
        )
        time.sleep(5)  # give it some time to react
    # Ensure that the operator did not die on start, or during the operation.
    assert runner.exception is None
    assert runner.exit_code == 0
    # There are usually more than these messages, but we only check for the certain ones.
    assert (
        "[mast/test-vars-ext-repo] Handler 'mastrun_handler' succeeded."
        in runner.stdout
    )
    assert (
        "[mast/test-vars-ext-repo] Creation is processed: 1 succeeded; 0 failed."
        in runner.stdout
    )

    workflow_name = subprocess.run(
        [
            "kubectl",
            "get",
            "mastrun",
            "test-vars-ext-repo",
            "--template={{.status.mastrun_handler.workFlow}}",
        ],
        shell=False,
        check=True,
        timeout=10,
        capture_output=True,
    )

    wait_for_workflow_to_finish(workflow_name.stdout.decode("utf-8"))

    workflow_status = subprocess.run(
        ["kubectl", "get", "wf", workflow_name.stdout.decode("utf-8")],
        shell=False,
        check=True,
        timeout=10,
        capture_output=True,
    )

    assert "Succeeded" in workflow_status.stdout.decode("utf-8")
