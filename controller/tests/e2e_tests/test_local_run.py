import subprocess
import time
import kopf.testing
import pytest
from tests.commons import TEST_MANIFESTS_DIR, wait_for_workflow_to_finish, MAST_SERVER

"""
Test to check run with all resources from configmaps
"""


mj_obj_yaml = TEST_MANIFESTS_DIR.joinpath("mj-local-run.yaml")

mr_obj_yaml = TEST_MANIFESTS_DIR.joinpath("mr-local-run.yaml")

inventory = TEST_MANIFESTS_DIR.joinpath("inventory-config.yaml")

playbook = TEST_MANIFESTS_DIR.joinpath("playbook-config.yaml")

variables = TEST_MANIFESTS_DIR.joinpath("vars-config.yaml")


@pytest.fixture(autouse=True)
def clean_after_test():
    yield
    # make sure objects dont exists so it could triiger mast server functions
    subprocess.run(["kubectl", "delete", "-f", mr_obj_yaml], check=True, shell=False)


def test_local_run():
    # To prevent lengthy threads in the loop executor when the process exits.
    settings = kopf.OperatorSettings()
    settings.watching.server_timeout = 10
    # Run an operator and simulate some activity with the operated resource.
    with kopf.testing.KopfRunner(
        ["run", "--all-namespaces", str(MAST_SERVER)],
        timeout=60,
        settings=settings,
    ) as runner:
        subprocess.run(
            ["kubectl", "apply", "-f", mj_obj_yaml], shell=False, check=True, timeout=10
        )
        time.sleep(1)  # give it some time to react
        subprocess.run(
            ["kubectl", "apply", "-f", playbook], shell=False, check=True, timeout=10
        )
        time.sleep(1)  # give it some time to react
        subprocess.run(
            ["kubectl", "apply", "-f", inventory], shell=False, check=True, timeout=10
        )
        time.sleep(1)  # give it some time to react
        subprocess.run(
            ["kubectl", "apply", "-f", variables], shell=False, check=True, timeout=10
        )
        time.sleep(1)  # give it some time to react
        subprocess.run(
            ["kubectl", "apply", "-f", mr_obj_yaml], shell=False, check=True, timeout=10
        )
        time.sleep(5)  # give it some time to react
    # Ensure that the operator did not die on start, or during the operation.
    assert runner.exception is None
    assert runner.exit_code == 0
    # There are usually more than these messages, but we only check for the certain ones.
    assert "[mast/test-local-run] Handler 'mastrun_handler' succeeded." in runner.stdout
    assert (
        "[mast/test-local-run] Creation is processed: 1 succeeded; 0 failed."
        in runner.stdout
    )

    workflow_name = subprocess.run(
        [
            "kubectl",
            "get",
            "mastrun",
            "test-local-run",
            "--template={{.status.mastrun_handler.workFlow}}",
        ],
        shell=False,
        check=True,
        timeout=10,
        capture_output=True,
        text=True,
    )

    wait_for_workflow_to_finish(workflow_name.stdout)

    workflow_status = subprocess.run(
        ["kubectl", "get", "wf", workflow_name.stdout],
        shell=False,
        check=True,
        timeout=10,
        capture_output=True,
        text=True,
    )

    assert "Succeeded" in workflow_status.stdout


@pytest.mark.parametrize(
    "missing_configmap,err_message",
    [
        (playbook, "Config map playbook-config does not exists"),
        (inventory, "Config map inventory-config does not exists"),
        (variables, "Config map vars-config does not exists"),
    ],
)
def test_no_config_map_exception(missing_configmap, err_message):
    # To prevent lengthy threads in the loop executor when the process exits.
    settings = kopf.OperatorSettings()
    settings.watching.server_timeout = 10
    # Run an operator and simulate some activity with the operated resource.
    with kopf.testing.KopfRunner(
        ["run", "--all-namespaces", str(MAST_SERVER)],
        timeout=60,
        settings=settings,
    ) as runner:
        subprocess.run(
            ["kubectl", "apply", "-f", mj_obj_yaml], shell=False, check=True, timeout=10
        )
        time.sleep(1)  # give it some time to react
        subprocess.run(
            ["kubectl", "apply", "-f", playbook], shell=False, check=True, timeout=10
        )
        subprocess.run(
            ["kubectl", "apply", "-f", inventory], shell=False, check=True, timeout=10
        )
        time.sleep(1)  # give it some time to react
        subprocess.run(
            ["kubectl", "apply", "-f", variables], shell=False, check=True, timeout=10
        )
        time.sleep(1)  # give it some time to react
        subprocess.run(
            ["kubectl", "delete", "-f", missing_configmap],
            shell=False,
            check=True,
            timeout=10,
        )
        time.sleep(1)  # give it some time to react
        subprocess.run(
            ["kubectl", "apply", "-f", mr_obj_yaml], shell=False, check=True, timeout=10
        )
        time.sleep(10)  # give it some time to react
    # Ensure that the operator did not die on start, or during the operation.
    assert runner.exception is None
    assert runner.exit_code == 0
    # There are usually more than these messages, but we only check for the certain ones.
    assert err_message in runner.stdout
    assert "[mast/test-local-run] Handler 'mastrun_handler' succeeded." in runner.stdout
    assert (
        "[mast/test-local-run] Creation is processed: 1 succeeded; 0 failed."
        in runner.stdout
    )

    mastrun_status = subprocess.run(
        [
            "kubectl",
            "get",
            "mastrun",
            "test-local-run",
            "--template={{.status.mastrun_handler}}",
        ],
        shell=False,
        check=True,
        timeout=10,
        capture_output=True,
        text=True,
    )

    assert "Failed" in mastrun_status.stdout
    assert "None" in mastrun_status.stdout
    assert err_message in mastrun_status.stdout
