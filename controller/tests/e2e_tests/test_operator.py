import subprocess
import time
import kopf.testing
import pytest
from tests.commons import TEST_MANIFESTS_DIR, MAST_SERVER

"""
Test to check if mr is triggered on certain object in k8s and ansible can manage k8s resources
Scenario:
- apply mast operator 
- apply mj i mr
- apply crd 'project'
- apply object of type 'project'
"""

mj_obj_yaml = TEST_MANIFESTS_DIR.joinpath("mj-operator.yaml")

mr_obj_yaml = TEST_MANIFESTS_DIR.joinpath("mr-operator.yaml")

mast_operator = TEST_MANIFESTS_DIR.joinpath("project-operator.yaml")

project_crd = TEST_MANIFESTS_DIR.joinpath("project-crd.yaml")

playbook = TEST_MANIFESTS_DIR.joinpath("operator-playbook.yaml")

project_obj = TEST_MANIFESTS_DIR.joinpath("project.yaml")

rbac = TEST_MANIFESTS_DIR.joinpath("operator-project-rbac.yaml")


# @pytest.fixture(autouse=True)
# def obj_absent():
#     # make sure objects dont exists so it could triiger mast server functions
#     yield
#     subprocess.run(
#         ["kubectl", "delete", "-f", mr_obj_yaml], check=True, timeout=10, shell=False
#     )

#     subprocess.run(
#         ["kubectl", "delete", "-f", project_obj], check=True, timeout=10, shell=False
#     )

#     subprocess.run(
#         ["kubectl", "delete", "ns", "myapp"], check=True, timeout=10, shell=False
#     )


def test_operator():

    # To prevent lengthy threads in the loop executor when the process exits.
    settings = kopf.OperatorSettings()
    settings.watching.server_timeout = 10

    # Run an operator and simulate some activity with the operated resource.
    with kopf.testing.KopfRunner(
        ["run", "--all-namespaces", str(MAST_SERVER)],
        timeout=60,
        settings=settings,
    ) as runner:
        subprocess.run(
            ["kubectl", "apply", "-f", mj_obj_yaml], shell=False, capture_output=True
        )
        time.sleep(1)
        subprocess.run(
            ["kubectl", "apply", "-f", playbook], shell=False, capture_output=True
        )
        time.sleep(1)  # give it some time to react
        subprocess.run(
            ["kubectl", "apply", "-f", mr_obj_yaml],
            shell=False,
            capture_output=True,
            check=True,
        )
        time.sleep(1)
        subprocess.run(
            ["kubectl", "apply", "-f", project_crd], shell=False, capture_output=True
        )
        time.sleep(1)
        subprocess.run(
            ["kubectl", "apply", "-f", rbac], shell=False, capture_output=True
        )
        time.sleep(1)
        subprocess.run(
            ["kubectl", "apply", "-f", mast_operator], shell=False, capture_output=True
        )
        time.sleep(5)

        subprocess.run(
            ["kubectl", "apply", "-f", project_obj],
            shell=False,
            check=True,
            timeout=10,
            capture_output=True,
        )
        time.sleep(120)  # give it some time to react

    # Ensure that the operator did not die on start, or during the operation.
    assert runner.exception is None
    assert runner.exit_code == 0

    ns = subprocess.run(
        ["kubectl", "get", "ns", "myapp"], shell=False, timeout=10, capture_output=True
    )

    assert "myapp" in ns.stdout.decode("utf-8")
