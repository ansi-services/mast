import subprocess
import time
import kopf.testing
import pytest
from tests.commons import TEST_MANIFESTS_DIR, MAST_SERVER, wait_for_workflow_to_finish

"""
Test to check if variables defined in mastjob or mastrun are filled from object triggering mastoperator
scenario:
- apply crd 'java-app'
- apply mast operator 
- apply mj i mr
- apply object of type 'java-app'
- load variables from java-app obj to mastjob ex. gti branch or name
"""

rbac = TEST_MANIFESTS_DIR.joinpath("operator-project-rbac.yaml")

java_app_crd = TEST_MANIFESTS_DIR.joinpath("java-app-crd.yaml", "")

playbook = TEST_MANIFESTS_DIR.joinpath("playbook-operator-variables.yaml")

mj = TEST_MANIFESTS_DIR.joinpath("mj-operator-variables.yaml")

mr = TEST_MANIFESTS_DIR.joinpath("mr-operator-variables.yaml")

mast_operator = TEST_MANIFESTS_DIR.joinpath("mo-javaapp.yaml")

java_app = TEST_MANIFESTS_DIR.joinpath("java-app.yaml")


@pytest.fixture(autouse=True)
def obj_absent():
    # make sure objects dont exists so it could triiger mast server functions
    yield

    subprocess.run(["kubectl", "delete", "-f", java_app], timeout=10, shell=False)
    time.sleep(2)

    subprocess.run(
        ["kubectl", "delete", "deployment", "spring-app"], timeout=10, shell=False
    )


def test_operator_pass_variables_from_triggering_obj():
    # To prevent lengthy threads in the loop executor when the process exits.
    settings = kopf.OperatorSettings()
    settings.watching.server_timeout = 10

    # Run an operator and simulate some activity with the operated resource.
    with kopf.testing.KopfRunner(
        ["run", "--all-namespaces", str(MAST_SERVER)],
        timeout=60,
        settings=settings,
    ) as runner:
        subprocess.run(
            ["kubectl", "apply", "-f", rbac], shell=False, capture_output=True
        )
        time.sleep(1)
        subprocess.run(
            ["kubectl", "apply", "-f", java_app_crd], shell=False, capture_output=True
        )
        time.sleep(3)  # give it some time to react
        subprocess.run(
            ["kubectl", "apply", "-f", playbook], shell=False, capture_output=True
        )
        time.sleep(1)
        subprocess.run(["kubectl", "apply", "-f", mj], shell=False, capture_output=True)
        time.sleep(1)
        subprocess.run(["kubectl", "apply", "-f", mr], shell=False, capture_output=True)
        time.sleep(1)
        subprocess.run(
            ["kubectl", "apply", "-f", mast_operator], shell=False, capture_output=True
        )
        time.sleep(5)

        subprocess.run(
            ["kubectl", "apply", "-f", java_app],
            shell=False,
            timeout=10,
            capture_output=True,
        )
        time.sleep(10)
        wofkflow_name = subprocess.run(
            [
                "kubectl",
                "get",
                "wf",
                "-l",
                "mastjob=javaapp-operator-mj",
                "-o",
                "jsonpath='{.items[0].metadata.name}'",
            ],
            shell=False,
            check=True,
            timeout=10,
            capture_output=True,
        )

        wait_for_workflow_to_finish(wofkflow_name.stdout.decode("utf-8").strip("'"))

        deploy = subprocess.run(
            ["kubectl", "get", "deploy", "spring-app"],
            check=True,
            shell=False,
            timeout=10,
            capture_output=True,
            text=True,
        )
    assert runner.exception is None
    assert runner.exit_code == 0
    assert "spring-app" in deploy.stdout
