import subprocess
import os
import time
import pytest
from tests.commons import TEST_MANIFESTS_DIR, CLI_DIR, ROOT_DIR, wait_for_argo_to_start


@pytest.fixture(autouse=True, scope="package")
def prepare_cluster():

    if not os.getenv("CI"):
        subprocess.run(
            [
                "kind",
                "create",
                "cluster",
                "--config",
                str(TEST_MANIFESTS_DIR.joinpath("kind-config.yaml")),
            ],
            shell=False,
            check=True,
            capture_output=True,
            encoding="utf-8",
        )


        subprocess.run(["go", "build", "."], cwd=str(CLI_DIR), shell=False, check=True, capture_output=True)
        # subprocess.run(["go", "build"], shell=False, check=True)

        subprocess.run(["chmod", "+x", str(CLI_DIR.joinpath("mast"))], shell=False, check=True, capture_output=True)

    # configure kubectl to work with kind cluster
    subprocess.run(
        ["kubectl", "cluster-info", "--context", "kind-kind"], shell=False, check=True
    )

    subprocess.run(
        [
            "kubectl",
            "apply",
            "-k",
            str(ROOT_DIR.joinpath("controller/manifests/overlays/test")),
        ],
        shell=False,
        check=True,
    )

    time.sleep(5)
    subprocess.run(
        ["kubectl", "config", "set-context", "kind-kind", "--namespace=mast"],
        shell=False,
        check=True,
    )

    wait_for_argo_to_start()

    # yield subprocess.run("kind delete cluster", check=True, shell=True)
