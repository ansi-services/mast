import subprocess
import time
import kopf.testing
import pytest
from tests.commons import TEST_MANIFESTS_DIR, MAST_SERVER, cleanup_mastjobs, cleanup_mastruns

"""
Test to check run with all resources from configmaps
"""

mj_obj_yaml = TEST_MANIFESTS_DIR.joinpath("mj-local-run.yaml")

mr_obj_yaml = TEST_MANIFESTS_DIR.joinpath("mr-local-run.yaml")

inventory = TEST_MANIFESTS_DIR.joinpath("inventory-config.yaml")

playbook = TEST_MANIFESTS_DIR.joinpath("playbook-config.yaml")

variables = TEST_MANIFESTS_DIR.joinpath("vars-config.yaml")

def test_update_mastruns_status(cleanup_mastjobs, cleanup_mastruns):

    # To prevent lengthy threads in the loop executor when the process exits.
    settings = kopf.OperatorSettings()
    settings.watching.server_timeout = 10

    # Run an operator and simulate some activity with the operated resource.
    with kopf.testing.KopfRunner(
        ["run", "--all-namespaces", str(MAST_SERVER)],
        timeout=60,
        settings=settings,
    ) as runner:

        subprocess.run(
            ["kubectl", "apply", "-f", mj_obj_yaml],
            shell=False,
            timeout=10,
            capture_output=True,
        )
        time.sleep(1)  # give it some time to react
        subprocess.run(
            ["kubectl", "apply", "-f", playbook],
            shell=False,
            timeout=10,
            capture_output=True,
        )
        time.sleep(1)  # give it some time to react
        subprocess.run(
            ["kubectl", "apply", "-f", inventory],
            shell=False,
            timeout=10,
            capture_output=True,
        )
        time.sleep(1)  # give it some time to react
        subprocess.run(
            ["kubectl", "apply", "-f", variables],
            shell=False,
            timeout=10,
            capture_output=True,
        )
        time.sleep(1)  # give it some time to react
        subprocess.run(
            ["kubectl", "apply", "-f", mr_obj_yaml],
            shell=False,
            check=True,
            timeout=10,
            capture_output=True,
        )
        time.sleep(40)  # give it some time to react

    # Ensure that the operator did not die on start, or during the operation.
    assert runner.exception is None
    assert runner.exit_code == 0
    # There are usually more than these messages, but we only check for the certain ones.
    assert "[mast/test-local-run] Handler 'mastrun_handler' succeeded." in runner.stdout
    assert (
        "[mast/test-local-run] Creation is processed: 1 succeeded; 0 failed."
        in runner.stdout
    )

    workflow_name = subprocess.run(
        [
            "kubectl",
            "get",
            "mastrun",
            "test-local-run",
            "--template={{.status.mastrun_handler.workFlow}}",
        ],
        shell=False,
        check=True,
        timeout=10,
        capture_output=True,
    )
    workflow_status = subprocess.run(
        [
            "kubectl",
            "get",
            "wf",
            workflow_name.stdout.decode("utf-8"),
            "--template={{.metadata.labels}}",
        ],
        shell=False,
        check=True,
        timeout=10,
        capture_output=True,
    )
    time.sleep(5)
    mastrun_status = subprocess.run(
        [
            "kubectl",
            "get",
            "mastrun",
            "test-local-run",
            "--template={{.status.mastrun_handler.status}}",
        ],
        shell=False,
        check=True,
        timeout=10,
        capture_output=True,
    )
    assert mastrun_status.stdout.decode("utf-8") in workflow_status.stdout.decode(
        "utf-8"
    )
