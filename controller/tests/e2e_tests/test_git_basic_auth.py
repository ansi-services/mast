import subprocess
import time
import kopf.testing
import pytest
from tests.commons import (
    TEST_MANIFESTS_DIR,
    MAST_SERVER,
    settings,
    wait_for_workflow_to_finish,
)

"""
Test the git authetntion with username and personal token acces(formally password)

create basic auth secret with username and personal token to remote git repository in order to run locally
"""

mj_obj_yaml = TEST_MANIFESTS_DIR.joinpath("mj-git-basic-auth.yaml")

mr_obj_yaml = TEST_MANIFESTS_DIR.joinpath("mr-git-basic-auth.yaml")

test_secret_basic_auth_obj_yaml = TEST_MANIFESTS_DIR.joinpath(
    "test-secret-git-basic-auth.yaml"
)


@pytest.fixture(autouse=True)
def clean_mr():
    # make sure objects dont exists so it could triiger mast server functions
    yield
    subprocess.run(
        ["kubectl", "delete", "-f", mr_obj_yaml],
        check=True,
        timeout=10,
        shell=False,
        capture_output=True,
    )


def test_git_basic_auth():

    # Run an operator and simulate some activity with the operated resource.
    with kopf.testing.KopfRunner(
        ["run", "--all-namespaces", str(MAST_SERVER)],
        timeout=60,
        settings=settings,
    ) as runner:

        subprocess.run(
            ["kubectl", "apply", "-f", mj_obj_yaml], check=True, shell=False, timeout=10
        )
        time.sleep(1)  # give it some time to react
        subprocess.run(
            ["kubectl", "apply", "-f", test_secret_basic_auth_obj_yaml],
            check=True,
            shell=False,
            timeout=20,
            capture_output=True,
        )
        time.sleep(1)  # give it some time to react
        subprocess.run(
            ["kubectl", "apply", "-f", mr_obj_yaml], shell=False, check=True, timeout=10
        )
        time.sleep(5)  # give it some time to react

    # Ensure that the operator did not die on start, or during the operation.
    assert runner.exception is None
    assert runner.exit_code == 0
    # There are usually more than these messages, but we only check for the certain ones.
    assert "[mast/git-basic-auth] Handler 'mastrun_handler' succeeded." in runner.stdout
    assert (
        "[mast/git-basic-auth] Creation is processed: 1 succeeded; 0 failed."
        in runner.stdout
    )

    workflow_name = subprocess.run(
        [
            "kubectl",
            "get",
            "mastrun",
            "git-basic-auth",
            "--template={{.status.mastrun_handler.workFlow}}",
        ],
        shell=False,
        check=True,
        timeout=10,
        capture_output=True,
    )

    wait_for_workflow_to_finish(workflow_name.stdout.decode("utf-8"))

    workflow_status = subprocess.run(
        [
            "kubectl",
            "get",
            "wf",
            workflow_name.stdout.decode("utf-8"),
            "--template={{.metadata.labels}}",
        ],
        shell=False,
        check=True,
        timeout=10,
        capture_output=True,
    )

    assert "Succeeded" in workflow_status.stdout.decode("utf-8")


def test_git_basic_auth_no_secret_exception():

    # Run an operator and simulate some activity with the operated resource.
    with kopf.testing.KopfRunner(
        ["run", "--all-namespaces", str(MAST_SERVER)],
        timeout=60,
        settings=settings,
    ) as runner:

        subprocess.run(["kubectl", "apply", "-f", mj_obj_yaml], shell=False, timeout=10)
        time.sleep(1)  # give it some time to react
        subprocess.run(
            ["kubectl", "delete", "-f", test_secret_basic_auth_obj_yaml],
            check=True,
            shell=False,
            timeout=10,
        )
        time.sleep(1)  # give it some time to react
        subprocess.run(
            ["kubectl", "apply", "-f", mr_obj_yaml], shell=False, check=True, timeout=10
        )
        time.sleep(10)  # give it some time to react

    # Ensure that the operator did not die on start, or during the operation.
    assert runner.exception is None
    assert runner.exit_code == 0
    # There are usually more than these messages, but we only check for the certain ones.
    assert "Secret test-secret-basic-auth does not exists" in runner.stdout
    assert "[mast/git-basic-auth] Handler 'mastrun_handler' succeeded." in runner.stdout
    assert (
        "[mast/git-basic-auth] Creation is processed: 1 succeeded; 0 failed."
        in runner.stdout
    )

    mastrun_status = subprocess.run(
        [
            "kubectl",
            "get",
            "mastrun",
            "git-basic-auth",
            "--template={{.status.mastrun_handler}}",
        ],
        shell=False,
        check=True,
        timeout=10,
        capture_output=True,
    )

    assert "Failed" in mastrun_status.stdout.decode("utf-8")
    assert "None" in mastrun_status.stdout.decode("utf-8")
    assert (
        "Secret test-secret-basic-auth does not exists"
        in mastrun_status.stdout.decode("utf-8")
    )
