# import os.path
# import subprocess
# import time
# import kopf.testing
# import pytest
# from commons import wait_for_workflow_to_finish, mast_server

# """
# Test the git authetntion with ssh key

# create ssh auth secret with ssh private key to remote git repository in order to run locally

# """

# mj_obj_yaml = os.path.relpath(
#     os.path.join(os.path.dirname(__file__),
#                  "./test_manifests", "mj-git-ssh-auth.yaml")
# )

# mr_obj_yaml = os.path.relpath(
#     os.path.join(os.path.dirname(__file__),
#                  "./test_manifests", "mr-git-ssh-auth.yaml")
# )

# test_secret_ssh_auth_obj_yaml = os.path.relpath(
#     os.path.join(
#         os.path.dirname(
#             __file__), "./test_manifests", "test-secret-git-ssh-auth.yaml"
#     )
# )


# @pytest.fixture(autouse=True)
# def obj_absent():
#     # make sure objects dont exists so it could triiger mast server functions
#     yield
#     subprocess.run(
#         f"kubectl delete -f {mr_obj_yaml}", check=True, timeout=10, shell=True
#     )


# def test_git_ssh_auth():

#     # To prevent lengthy threads in the loop executor when the process exits.
#     settings = kopf.OperatorSettings()
#     settings.watching.server_timeout = 10

#     # Run an operator and simulate some activity with the operated resource.
#     with kopf.testing.KopfRunner(
#         ["run", "--all-namespaces", mast_server],
#         timeout=60,
#         settings=settings,
#     ) as runner:

#         subprocess.run(
#             f"kubectl apply -f {mj_obj_yaml}", check=True, shell=True, timeout=30
#         )
#         time.sleep(1)  # give it some time to react
#         subprocess.run(
#             f"kubectl apply -f {test_secret_ssh_auth_obj_yaml}",
#             check=True,
#             shell=True,
#             timeout=30,
#         )
#         time.sleep(1)  # give it some time to react
#         subprocess.run(
#             f"kubectl apply -f {mr_obj_yaml}", shell=True, check=True, timeout=30
#         )
#         time.sleep(5)  # give it some time to react

#     # Ensure that the operator did not die on start, or during the operation.
#     assert runner.exception is None
#     assert runner.exit_code == 0
#     # There are usually more than these messages, but we only check for the certain ones.
#     assert "[mast/git-ssh-auth] Handler 'mastrun_handler' succeeded." in runner.stdout
#     assert (
#         "[mast/git-ssh-auth] Creation is processed: 1 succeeded; 0 failed."
#         in runner.stdout
#     )

#     workflow_name = subprocess.run(
#         "kubectl get mastrun git-ssh-auth --template={{.status.mastrun_handler.workFlow}}",
#         shell=True,
#         check=True,
#         timeout=10,
#         capture_output=True,
#     )

#     wait_for_workflow_to_finish(workflow_name.stdout.decode("utf-8"))

#     workflow_status = subprocess.run(
#         f"kubectl get wf {workflow_name.stdout.decode('utf-8')",
#         shell=True,
#         check=True,
#         timeout=10,
#         capture_output=True,
#     )

#     assert "Succeeded" in workflow_status.stdout.decode("utf-8")
