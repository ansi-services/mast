import yaml
from src.utils import merge_and_override_dict

mj = """
apiVersion: mast.ansi.services/v1
kind: MastJob
metadata:
  name: mj-vars-override
  namespace: mast
spec:
  ansible:
    version: 2.9.18
  workflowName: my-wf
  configuration:
    playbook:
      - fromConfigMap:
          name: playbook-config3
          key: playbook.yaml
    vars:
      - fromConfigMap:
          name: vars-config
          key: vars.yaml
      - fromExtRepo:
          git:
            path: vars.yaml
            url: https://github.com/smokaleksander/test-repo.git
      - fromLiteral:
          - my_extra_repo_var: i am literal override
    inventory:
      - fromConfigMap:
          name: inventory-config
          key: inventory
"""

mr = """
apiVersion: mast.ansi.services/v1
kind: MastRun
metadata:
  name: mr-vars-override
spec:
  mastJobRef:
    name: mj-vars-override
    specOverride:
      ansible:
        version: latest
      workflowName: new-wf
      configuration:
        playbook:
          - fromConfigMap:
              name: playbook-config2
              key: playbook-new.yaml
        vars:
          - fromConfigMap:
              name: vars-config2
              key: vars-new.yaml
              """


mj_yaml = yaml.safe_load(mj)
mr_yaml = yaml.safe_load(mr)


def test_merge_and_override_dict():
    """test merge and override dict"""

    mj_yaml["spec"] = merge_and_override_dict(
        mj_yaml["spec"], mr_yaml["spec"]["mastJobRef"]["specOverride"]
    )
    print(mj_yaml)
    # check if var is preserved
    assert (
        mj_yaml["spec"]["configuration"]["vars"][1]["fromExtRepo"]["git"]["url"]
        == "https://github.com/smokaleksander/test-repo.git"
    )
    # check if var is added
    assert (
        mj_yaml["spec"]["configuration"]["vars"][3]["fromConfigMap"]["name"]
        == "vars-config2"
        and mj_yaml["spec"]["configuration"]["vars"][3]["fromConfigMap"]["key"]
        == "vars-new.yaml"
    )
    # check if playbook is overridden
    assert (
        mj_yaml["spec"]["configuration"]["playbook"][0]["fromConfigMap"]["name"]
        == "playbook-config2"
        and mj_yaml["spec"]["configuration"]["playbook"][0]["fromConfigMap"]["key"]
        == "playbook-new.yaml"
    )
    assert (
        mj_yaml["spec"]["workflowName"] == "new-wf"
    )
