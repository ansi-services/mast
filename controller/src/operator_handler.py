from collections import defaultdict
from collections.abc import Iterable
from pydantic import BaseModel

class OperatorMeta(BaseModel):
    name: str
    mastrun: str

    @classmethod
    def from_spec(cls, name, spec, action_ref = "onEvent"):
        return cls.parse_obj({
                "mastrun": spec["run"]["mastRunRef"][action_ref],
                "name": name,
            })


def unique_handlers(operators: Iterable) -> Iterable:
    run_refs = []
    for operator in operators:
        if operator.mastrun not in run_refs:
            run_refs.append(operator.mastrun)
            yield operator


action_to_handler = lambda action: "on{}".format(action.capitalize())


class OperatorHandler(object):

    _handled_resources: defaultdict

    def __init__(self):
        # structure is complicated for fast find operation as it's dominated function
        self._handled_resources = defaultdict(lambda: defaultdict(lambda: defaultdict(list)))

    def append(self, obj: str, spec:str , name: str):
        # we append data in order that will simplify obtaining it without iteration

        actions = spec["event"]["actions"]
        namespaces = spec["event"]["namespaces"]
        run_events_types = spec["run"]["mastRunRef"].keys()
        is_generic_handler = "onEvent" in run_events_types

        if not is_generic_handler and \
           not all(action_to_handler(action) in run_events_types for action in actions):
            raise LookupError

        for action in actions:
            action_ref = "onEvent" if is_generic_handler else action_to_handler(action)

            for namespace in namespaces:
                self._handled_resources[obj][action][namespace].append(
                    OperatorMeta.from_spec(name, spec, action_ref)
                )

    def delete(self, obj: str, name: str):
        was_removed = False
        if self._handled_resources.get(obj):
            for action, namespaces in self._handled_resources[obj].items():
                for namespace, operators in namespaces.items():
                    for mast_operator in operators:
                        if mast_operator.name == name:
                            was_removed = True
                            self._handled_resources[obj][action][namespace].remove(mast_operator)
            if not was_removed:
                raise LookupError()
        else:
            raise LookupError()

    def update(self, obj: str, spec: dict, name: str):
        self.delete(obj, name)
        self.append(obj, spec, name)

    def has_operators_for_scope(self, obj: str, namespace: str,action: str) -> bool:
        return len(list(self.find(obj, namespace, action))) > 0

    def find(self, obj: str, namespace: str, action:str) -> Iterable:
        if obj not in self._handled_resources or \
                            action not in self._handled_resources[obj]:
            return []

        result = []

        namespaces = self._handled_resources[obj][action]
        all_namespaced = namespaces.get("all")
        if all_namespaced:
            result = all_namespaced

        namespaced = namespaces.get(namespace)
        if namespaced:
            result += namespaced

        return unique_handlers(result)
