import yaml
import random
import string
import kopf
import os
from typing import Dict
from kubernetes import client, config, utils
from kubernetes.client.rest import ApiException
from jinja2 import Environment, FileSystemLoader, Template
from src.static_variables import (
    NAMESPACE,
    MAST_GROUP,
    MAST_API_VERSION,
    ANSIBLE_IMAGE,
    K8S_API_CLIENT,
    EventType,
    INTERVAL_BETWEEN_RETRIES,
)


def generate_random_string(length: int) -> str:
    return "".join(random.choices(string.ascii_lowercase + string.digits, k=length))


def render_variables_from_resource(mastjob, resource) -> Dict:
    # convert dict to yaml
    mastjob_yaml = yaml.dump(mastjob)
    mastjob_j2tmpl = Template(mastjob_yaml)
    mastjob_rendered = mastjob_j2tmpl.render(resource=resource)
    mastjob_rendered_dict = yaml.safe_load(mastjob_rendered)
    return mastjob_rendered_dict


def merge_and_override_dict(base_dict, override_dict) -> Dict:
    new_dict = base_dict.copy()

    for key in override_dict:
        if (
            key in new_dict
            and isinstance(new_dict[key], dict)
            and isinstance(override_dict[key], dict)
        ):
            new_dict[key] = merge_and_override_dict(new_dict[key], override_dict[key])
        elif key == "vars":
            new_dict[key].extend(override_dict[key])
        else:
            new_dict[key] = override_dict[key]

    return new_dict


def is_of_interest(resource, memo, namespace, event, **_) -> bool:
    return memo.operator_handler.has_operators_for_scope(
        resource.plural, namespace, EventType(event.get("type")).name
    )


def validate_configmap(configmap_name: str):
    # check if specified configmap exisits
    # if not Kopf.TemporaryError is thrown which means
    # kopf will try to retrie the whole function for 3 times iwth 60s interval in between
    coreV1Api = client.CoreV1Api(K8S_API_CLIENT)
    try:
        response = coreV1Api.read_namespaced_config_map(
            name=configmap_name, namespace=NAMESPACE
        )
    except ApiException as e:
        raise kopf.TemporaryError(
            "Config map {} does not exists".format(configmap_name),
            delay=INTERVAL_BETWEEN_RETRIES,
        )


def validate_secret(secret_name: str):
    # check if specified secret exisits
    # if not Kopf.TemporaryError is thrown which means
    # kopf will try to retrie the whole function for 3 times iwth 60s interval in between
    coreV1Api = client.CoreV1Api(K8S_API_CLIENT)
    try:
        response = coreV1Api.read_namespaced_secret(
            name=secret_name, namespace=NAMESPACE
        )
    except ApiException as e:
        raise kopf.TemporaryError(
            "Secret {} does not exists, {}".format(secret_name, e),
            delay=INTERVAL_BETWEEN_RETRIES,
        )


def get_repo(git: Dict) -> Dict:
    repo = {}
    repo["name"] = f"repo-{generate_random_string(8)}"
    repo["path"] = repo["name"]
    repo["url"] = git.get("url", None)
    repo["revision"] = git.get("revision", "main")
    repo["workDir"] = git.get("workDir", "root")
    if git.get("credentials", False):
        if git["credentials"].get("ssh", False):
            try:
                validate_secret(
                    git["credentials"]["ssh"]["sshPrivateKeySecret"].get("name")
                )
            except kopf.PermanentError as e:
                raise e
            repo["credentials"] = git["credentials"].get("ssh")
        else:
            validate_secret(
                git["credentials"]["basicAuth"]["usernameSecret"].get("name")
            )
            validate_secret(
                git["credentials"]["basicAuth"]["passwordSecret"].get("name")
            )
            repo["credentials"] = git["credentials"].get("basicAuth")
    return repo


def create_workflow_template(mastjob_body: Dict, mastrun_body: Dict, logger) -> Dict:
    isExtraVariables = False
    # get the template
    env = Environment(
        loader=FileSystemLoader(
            os.path.abspath(os.path.dirname(__file__)) + "/templates", encoding="utf8"
        ),
        trim_blocks=True,
        lstrip_blocks=True,
    )
    template = env.get_template("workflow_tmpl.j2")
    # run default image or created by user, version is setting by default in mastjob
    if mastjob_body["spec"]["ansible"].get("version"):
        image = f'{ANSIBLE_IMAGE}:{mastjob_body["spec"]["ansible"].get("version")}'
    else:
        image = mastjob_body["spec"]["ansible"].get("image")

    workflowName = mastjob_body["spec"].get("workflowName", None)

    mastrun_name = mastrun_body["metadata"].get("name")
    mastjob_name = mastjob_body["metadata"].get("name")
    service_account = mastjob_body["spec"].get("serviceAccount")

    repo_artifacts = []
    inventories = []
    playbooks = []
    variables = []
    ansible_playbook_args = ""
    # repository source
    if mastjob_body["spec"].get("repository", None):
        try:
            repo_artifacts.append(
                get_repo(mastjob_body["spec"]["repository"].get("git"))
            )
        except kopf.PermanentError as e:
            raise e

    # playbooks sources
    for playbook in mastjob_body["spec"]["configuration"]["playbook"]:
        if playbook.get("fromRepo"):
            playbook["fromRepo"]["repo_path"] = repo_artifacts[0]["path"]
            ansible_playbook_args = f'"{playbook["fromRepo"]["path"]}",'
        if playbook.get("fromConfigMap"):
            try:
                validate_configmap(playbook["fromConfigMap"].get("name"))
            except kopf.PermanentError as e:
                raise e
            ansible_playbook_args = f'"{playbook["fromConfigMap"]["key"]}",'
        playbooks.append(playbook)

    # inventory sources
    if mastjob_body["spec"]["configuration"].get("inventory", False):
        for inv in mastjob_body["spec"]["configuration"]["inventory"]:
            if inv.get("fromRepo"):
                inv["fromRepo"]["repo_path"] = repo_artifacts[0]["path"]
                ansible_playbook_args += f' "-i", "{inv["fromRepo"]["path"]}",'
            if inv.get("fromConfigMap"):
                try:
                    validate_configmap(inv["fromConfigMap"].get("name"))
                except kopf.PermanentError as e:
                    raise e
                ansible_playbook_args += f' "-i", "{inv["fromConfigMap"]["key"]}",'
            inventories.append(inv)

    # vars sources
    num_of_ext_repo = 0
    if mastjob_body["spec"]["configuration"].get("vars", False):
        for var in mastjob_body["spec"]["configuration"]["vars"]:
            if var.get("fromRepo"):
                var["fromRepo"]["repo_path"] = repo_artifacts[0]["path"]
                variables.append(var)
            elif var.get("fromExtRepo"):
                extrepo = get_repo(var["fromExtRepo"].get("git"))
                var["fromExtRepo"]["repo_path"] = extrepo["path"]
                variables.append(var)
                repo_artifacts.append(extrepo)
                isExtraVariables = True
            elif var.get("fromConfigMap"):
                try:
                    validate_configmap(var["fromConfigMap"].get("name"))
                except kopf.PermanentError as e:
                    raise e
                variables.append(var)
                isExtraVariables = True
            else:
                variables.append(var)
                isExtraVariables = True

    # always run with extra_vars, if empty nothing will happen
    if isExtraVariables:
        ansible_playbook_args += '"-e","@mast_extra_vars.yaml"'
    # load template with vars and save to file

    workflowtemplate_definition = template.render(
        workflowName=workflowName,
        mastjob_name=mastjob_name,
        mastrun_name=mastrun_name,
        image=image,
        service_account=service_account,
        repo_artifacts=repo_artifacts,
        playbooks=playbooks,
        variables=variables,
        inventories=inventories,
        ansible_playbook_args=ansible_playbook_args,
    )

    workflow_yaml: Dict = yaml.safe_load(workflowtemplate_definition)
    return workflow_yaml


def get_mast_object(plural_kind, name) -> Dict:
    K8S_API_CLIENT = client.ApiClient()
    customObjectApi = client.CustomObjectsApi(K8S_API_CLIENT)
    try:
        obj: Dict = customObjectApi.get_namespaced_custom_object(
            group=MAST_GROUP,
            version=MAST_API_VERSION,
            namespace=NAMESPACE,
            plural=plural_kind,
            name=name,
        )
    except ApiException as e:
        raise kopf.PermanentError(e)
        # "{} {} does not exists".format(plural_kind, name))
    return obj
