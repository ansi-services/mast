import enum

NAMESPACE = "mast"

ARGO_GROUP = "argoproj.io"
ARGO_API_VERSION = "v1alpha1"

MAST_GROUP = "mast.ansi.services"
MAST_API_VERSION = "v1"

ANSIBLE_IMAGE = "registry.gitlab.com/ansi-services/mast/ansible"
K8S_API_CLIENT = None

NUMBER_OF_KOPF_RETRIES=3
INTERVAL_BETWEEN_RETRIES=60

class EventType(enum.Enum):
    # CRUD name = kopf event type
    create = "ADDED"
    update = "MODIFIED"
    delete = "DELETED"
    # handling None type in kopf
    invalid = None
