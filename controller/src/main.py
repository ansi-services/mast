from typing import Dict
import kopf
from kubernetes import client, config, utils
from kubernetes.client.rest import ApiException

from operator_handler import OperatorHandler

from static_variables import (
    NAMESPACE,
    ARGO_GROUP,
    ARGO_API_VERSION,
    MAST_GROUP,
    MAST_API_VERSION,
    K8S_API_CLIENT,
    EventType,
    NUMBER_OF_KOPF_RETRIES,
    INTERVAL_BETWEEN_RETRIES,
)
from utils import (
    generate_random_string,
    render_variables_from_resource,
    merge_and_override_dict,
    is_of_interest,
    create_workflow_template,
    get_mast_object,
)


@kopf.on.startup()
def init(memo, **_):
    memo.operator_handler = OperatorHandler()
    K8S_API_CLIENT = client.ApiClient()


@kopf.on.create("mastoperators")
@kopf.on.resume("mastoperators")
def register_mast_operator(name, spec, memo, **_):
    target_objects = spec["event"]["objects"]
    for obj in target_objects:
        memo.operator_handler.append(obj, spec, name)


@kopf.on.update("mastoperators")
def update_mast_operator(name, spec, memo, **_):
    target_objects = spec["event"]["objects"]
    for obj in target_objects:
        memo.operator_handler.update(obj, spec, name)


@kopf.on.delete("mastoperators")
def unregister_mast_operator(name, spec, memo, **_):
    target_objects = spec["event"]["objects"]
    for obj in target_objects:
        memo.operator_handler.delete(obj, name)


@kopf.on.event(kopf.EVERYTHING, when=is_of_interest)
def operator_resources_watcher(
    resource, name, namespace, body, event, memo, logger, **_
):
    mast_operator_details = memo.operator_handler.find(
        resource.plural, namespace, EventType(event.get("type")).name
    )

    for operator in mast_operator_details:
        mastrun_body_template = get_mast_object("mastruns", operator.mastrun)

        mastrun_body = {
            "apiVersion": "mast.ansi.services/v1",
            "kind": "MastRun",
            "metadata": {
                "name": f"{mastrun_body_template['metadata']['name']}-{name}-{generate_random_string(4)}",
                "namespace": "mast",
            },
            "spec": mastrun_body_template["spec"],
        }
        mastrun_body["spec"]["mastOperatorObj"] = dict(body)
        mastrun_body["spec"]["template"] = False

        try:
            customObjectApi = client.CustomObjectsApi(K8S_API_CLIENT)
            obj: Dict = customObjectApi.create_namespaced_custom_object(
                group=MAST_GROUP,
                version=MAST_API_VERSION,
                namespace=NAMESPACE,
                plural="mastruns",
                body=mastrun_body,
            )
            logger.info(f'Mastrun {obj["metadata"]["name"]} created')
        except ApiException as e:
            raise kopf.TemporaryError(
                f"Creating mastrun failed, {e.body['message']}, retries in 10s",
                delay=INTERVAL_BETWEEN_RETRIES,
            )
        # mastrun_handler(mastrun_body, logger)
        logger.info(
            f"Mast operator {operator.name} triggered {mastrun_body['metadata']['name']} for {resource.singular} {name}"
        )


@kopf.on.create("mastruns", retries=NUMBER_OF_KOPF_RETRIES)
def mastrun_handler(name, body, logger, **_):
    # check if mastrun is a template
    if body["spec"].get("template") == True:
        logger.info(f"MastRun {name} is a template, skipping")
        return

    mastjob_name: str = body["spec"]["mastJobRef"].get("name")

    # if MastJob specified in MastRun dont exisits, Kopf.TemporaryError is thrown which means kopf will try to retrie the whole function for 3 times iwth 60s
    try:
        mastjob = get_mast_object("mastjobs", mastjob_name)
    except kopf.TemporaryError as e:
        logger.error(str(e))
        return {
            "mastJob": "None",
            "workFlow": "None",
            "status": "Failed",
            "message": str(e),
        }

    if body["spec"]["mastJobRef"].get("specOverride", None):
        mastjob["spec"] = merge_and_override_dict(
            mastjob["spec"], body["spec"]["mastJobRef"]["specOverride"]
        )
    if body["spec"].get("mastOperatorObj"):
        mastjob = render_variables_from_resource(
            mastjob, body["spec"]["mastOperatorObj"]
        )

    try:
        workflow: Dict = create_workflow_template(
            mastjob_body=mastjob, mastrun_body=body, logger=logger
        )
    except kopf.TemporaryError as e:
        logger.error(str(e))
        return {
            "mastJob": mastjob_name,
            "workFlow": "None",
            "status": "Failed",
            "message": str(e),
        }

    # mastrun parent to workfow
    kopf.adopt(workflow)
    try:
        # K8S_API_CLIENT = client.ApiClient()
        customObjectApi = client.CustomObjectsApi(K8S_API_CLIENT)
        obj: Dict = customObjectApi.create_namespaced_custom_object(
            group=ARGO_GROUP,
            version=ARGO_API_VERSION,
            namespace=NAMESPACE,
            plural="workflows",
            body=workflow,
        )
    except ApiException as e:
        raise kopf.TemporaryError("Creating workflow failed, retries in 10s", delay=10)
    logger.debug("Workflow started")
    # return data to save in references in mastrun
    return {
        "mastJob": mastjob_name,
        "workFlow": obj["metadata"]["name"],
        "status": "Running",
    }


@kopf.on.field("workflows", field="metadata.labels", retries=NUMBER_OF_KOPF_RETRIES)
def update_workflows_status_in_mastrun(old, new, name, body, **_):
    status = new.get("workflows.argoproj.io/phase", None)
    mastrun_name = new.get("mastrun", None)
    if status is None or mastrun_name is None:
        return
    mastrun_name = new["mastrun"]
    # get mastrun body
    mastrun_body = get_mast_object("mastruns", mastrun_name)
    # update mastrun status
    try:
        mastrun_body["status"]["mastrun_handler"]["status"] = status
    except KeyError:
        return

    if status == "Failed":
        message = body["status"].get("message")
        mastrun_body["status"]["mastrun_handler"]["message"] = message

    try:
        customObjectApi = client.CustomObjectsApi(K8S_API_CLIENT)
        obj: Dict = customObjectApi.patch_namespaced_custom_object(
            group=MAST_GROUP,
            version=MAST_API_VERSION,
            namespace=NAMESPACE,
            name=mastrun_name,
            plural="mastruns",
            body=mastrun_body,
        )
    except ApiException as e:
        # if patch wont be success, Kopf.TemporaryError is thrown which means kopf will try to retrie the whole function for 3 times iwth 60s
        raise kopf.TemporaryError(
            "Updating mastrun`s workflow status failed, retrying in 10s",
            delay=INTERVAL_BETWEEN_RETRIES,
        )
