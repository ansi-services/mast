FROM debian:11.3

ARG PYTHON_VERSION=3.7.0
ARG KIND_VERSION=v0.11.1
ARG KUBECTL_VERSION=v1.22.4
ARG PYENV_HOME=/root/.pyenv

RUN apt-get update && \
    apt-get install ca-certificates curl wget gnupg lsb-release apt-utils git build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libsqlite3-dev libreadline-dev libffi-dev curl libbz2-dev gcc -y && \
    apt-get clean

RUN curl -fsSL https://get.docker.com -o get-docker.sh && \
    sh get-docker.sh

RUN wget -O /usr/local/bin/kind https://github.com/kubernetes-sigs/kind/releases/download/${KIND_VERSION}/kind-linux-amd64 && chmod +x /usr/local/bin/kind
RUN wget -O /usr/local/bin/kubectl https://storage.googleapis.com/kubernetes-release/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl && chmod +x /usr/local/bin/kubectl

RUN git clone --depth 1 https://github.com/pyenv/pyenv.git $PYENV_HOME && rm -rfv $PYENV_HOME/.git && \
    export PATH=${PYENV_HOME}/shims:${PYENV_HOME}/bin:$PATH && \
    pyenv install ${PYTHON_VERSION} && \
    pyenv global ${PYTHON_VERSION} && \
    eval "$(pyenv init -)" && \
    pip3 install --upgrade pip && pyenv rehash && \
    pip3 install pipenv
