# Changelog

## Version 0.1

*  operator support for simple repository based MastJob
*  operator support for simple configMap based MastJob
*  operator support for MastRun to override configuration of MastJob
