package cmd

import (
	"context"
	"os"

	"github.com/olekukonko/tablewriter"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func init() {
	operatorCmd.AddCommand(operatorListCmd)
	log.SetLevel(log.InfoLevel)
}

var operatorListCmd = &cobra.Command{
	Use:   "list",
	Short: "Show list of mast operators",
	Run: func(cmd *cobra.Command, args []string) {

		if Verbose {
			log.SetLevel(log.DebugLevel)
		}

		dynamicClient := connectToK8s()

		operators, err := dynamicClient.Resource(mastOperatorGVR).Namespace("mast").List(context.TODO(), metav1.ListOptions{})
		if err != nil {
			log.Fatal(err)
			return
		}

		if len(operators.Items) > 0 {
			table := tablewriter.NewWriter(os.Stdout)
			table.SetHeader([]string{"NAME", "	AGE", "MASTJOB"})
			setTableConfigs(table)
			for _, operator := range operators.Items {
				table.Append([]string{operator.GetName(), getResourceAge(operator)})
			}
			table.Render()
		} else {
			log.Info("No must operators found")
		}

	},
}
