package cmd

import (
	"context"
	"os"

	"github.com/olekukonko/tablewriter"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func init() {
	jobCmd.AddCommand(jobListCmd)
	log.SetLevel(log.InfoLevel)
}

var jobListCmd = &cobra.Command{
	Use:   "list",
	Short: "Show list of mast jobs",
	Run: func(cmd *cobra.Command, args []string) {
		if Verbose {
			log.SetLevel(log.DebugLevel)
		}

		dynamicClient := connectToK8s()

		mastJobs, err := dynamicClient.Resource(mastJobGVR).Namespace(defaultNamespace).List(context.TODO(), metav1.ListOptions{})
		if err != nil {
			log.Fatal("failed to fetch mast jobs")

		}

		if len(mastJobs.Items) > 0 {
			table := tablewriter.NewWriter(os.Stdout)
			table.SetHeader([]string{"NAME", "AGE"})
			setTableConfigs(table)
			for _, mastRun := range mastJobs.Items {
				table.Append([]string{mastRun.GetName(), getResourceAge(mastRun)})
			}
			table.Render()
			os.Exit(0)
		} else {
			log.Info("No must jobs found")
			os.Exit(0)
		}

	},
}
