package cmd

import (
	"context"
	"os"
	"strings"

	"github.com/olekukonko/tablewriter"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
)

func init() {
	runCmd.AddCommand(runListCmd)
	log.SetLevel(log.InfoLevel)
}

var runListCmd = &cobra.Command{
	Use:   "list",
	Short: "Show list of mast runs",
	Run: func(cmd *cobra.Command, args []string) {
		if Verbose {
			log.SetLevel(log.DebugLevel)
		}

		dynamicClient := connectToK8s()

		mastRuns, err := dynamicClient.Resource(mastRunGVR).Namespace("mast").List(context.TODO(), metav1.ListOptions{})
		if err != nil {
			log.Fatal("Fetching MastRuns failed")
		}
		log.Debug("Mastruns fetched")
		workflows, err := dynamicClient.Resource(workflowGVR).Namespace("mast").List(context.TODO(), metav1.ListOptions{})
		if err != nil {
			log.Fatal("Fetching Workflows failed")
		}
		log.Debug("Workflows fetched")
		var workFlowName string

		if len(mastRuns.Items) > 0 {
			table := tablewriter.NewWriter(os.Stdout)
			setTableConfigs(table)
			table.SetHeader([]string{"NAME", "STATUS", "AGE"})

			for a := 0; a < len(mastRuns.Items); a++ {
				var isWorkFlowName bool
				var err error
				workFlowName, isWorkFlowName, err = unstructured.NestedString(mastRuns.Items[a].Object, "status", "mastrun_handler", "workFlow")
				if err != nil {
					log.Fatal(err)
				} else if isWorkFlowName == false {
					table.Append([]string{mastRuns.Items[a].GetName(), "Template", getResourceAge(mastRuns.Items[a])})
				} else if workFlowName == "None" {
					table.Append([]string{mastRuns.Items[a].GetName(), "FAILED", getResourceAge(mastRuns.Items[a])})
				} else {
					for _, workflow := range workflows.Items {
						//match mastrun to workflow
						if workflow.GetName() == workFlowName {
							age := getResourceAge(mastRuns.Items[a])
							var status string
							var isStatusFound bool
							var err error
							isCompleted, isCompletedFound, err := unstructured.NestedString(workflow.Object, "metadata", "labels", "workflows.argoproj.io/completed")
							if err != nil {
								log.Error("Failed to fetch Workflow completed status")
								log.Fatal(err)
							} else if isCompletedFound == false {
								status = "Running"
								table.Append([]string{mastRuns.Items[a].GetName(), strings.ToUpper(status), age})
							} else if isCompleted == "true" {
								status, isStatusFound, err = unstructured.NestedString(workflow.Object, "metadata", "labels", "workflows.argoproj.io/phase")
								if err != nil {
									log.Fatal("Could not fetch workflow status")
								} else if isStatusFound == false {
									log.Fatal("Could not fetch workflow status")
								}
								table.Append([]string{mastRuns.Items[a].GetName(), strings.ToUpper(status), age})

							}

						}
					}
				}

			}
			table.Render()
			os.Exit(0)
		} else {
			log.Info("No MustRuns found")
			os.Exit(0)
		}

	},
}
