package cmd

import (
	"context"
	"os"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
)

func init() {
	jobCmd.AddCommand(jobCreateCmd)
	log.SetLevel(log.InfoLevel)
}

var jobCreateCmd = &cobra.Command{
	Use:   "create [job file]",
	Short: "Create new mastjob",
	Long:  `Create new mastjob in mast namespace by providing job's file names`,
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		if Verbose {
			log.SetLevel(log.DebugLevel)
		}
		file := getObjectDef(args[0])
		log.Debug("file " + args[0] + " loaded")
		job := convertFileToMap(file)

		dynamicClient := connectToK8s()

		//convert yaml to json
		jobObject := &unstructured.Unstructured{job}
		//post new object to cluster
		result, err := dynamicClient.Resource(mastJobGVR).Namespace(defaultNamespace).Create(context.TODO(), jobObject, metav1.CreateOptions{})
		if err != nil {
			log.Fatal(err)
		}
		log.Info("MastJob ", result.GetName(), " created\n")
		os.Exit(0)
	},
}
