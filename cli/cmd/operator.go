package cmd

import (
	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(operatorCmd)
}

var operatorCmd = &cobra.Command{
	Use:   "operator",
	Short: "Perform operations on operators",
}
