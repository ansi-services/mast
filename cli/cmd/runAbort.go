package cmd

import (
	"context"

	log "github.com/sirupsen/logrus"

	"github.com/spf13/cobra"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
)

func init() {
	runCmd.AddCommand(runAbortCmd)
	log.SetLevel(log.InfoLevel)
}

var runAbortCmd = &cobra.Command{
	Use:   "abort [name]",
	Short: "abort mastrun",
	Long:  `abort mastrun by providing a path to run file or its name`,
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		if Verbose {
			log.SetLevel(log.DebugLevel)
		}

		dynamicClient := connectToK8s()

		mastrun, err := dynamicClient.Resource(mastRunGVR).Namespace("mast").Get(context.TODO(), args[0], metav1.GetOptions{})
		if err != nil {
			log.Fatal(err)
		}

		workFlowName, isWorkFlowName, err := unstructured.NestedString(mastrun.Object, "status", "mastrun_handler", "workFlow")
		if err != nil {
			log.Fatal(err)
		} else if isWorkFlowName == false {
			log.Error("Mastrun server is not working")
		} else if workFlowName == "None" {
			handlerMessage, isHandlerMessage, err := unstructured.NestedString(mastrun.Object, "status", "mastrun_handler", "message")
			if err != nil || isHandlerMessage == false {
				log.Fatal(err)
			}
			log.Info("MastRun did not started")
			log.Info(handlerMessage)
			return
		}
		workflow, err := dynamicClient.Resource(workflowGVR).Namespace("mast").Get(context.TODO(), workFlowName, metav1.GetOptions{})
		if err != nil {
			log.Fatal(err)
		}

		if err := unstructured.SetNestedField(workflow.Object, "Terminate", "spec", "shutdown"); err != nil {
			log.Fatal("Failed to terminate workflow", err)
		}

		_, updateErr := dynamicClient.Resource(workflowGVR).Namespace("mast").Update(context.TODO(), workflow, metav1.UpdateOptions{})
		if updateErr != nil {
			log.Fatal(err)
		}
		log.Info("MastRun ", args[0], " aborted")
	},
}
