package cmd

import (
	"context"

	log "github.com/sirupsen/logrus"

	"github.com/spf13/cobra"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
)

func init() {
	jobCmd.AddCommand(jobRunCmd)
	log.SetLevel(log.InfoLevel)
}

var jobRunCmd = &cobra.Command{
	Use:   "run [name]",
	Short: "Start a job",
	Long:  `Create a mast run from a job, this command generate a basic mast run object and submits it`,
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		if Verbose {
			log.SetLevel(log.DebugLevel)
		}

		dynamicClient := connectToK8s()

		mastjob, err := dynamicClient.Resource(mastJobGVR).Namespace("mast").Get(context.TODO(), args[0], metav1.GetOptions{})
		if err != nil {
			log.Fatal("MastJob ", args[0], " does not exist")
		}

		log.Debug("Creating new mast run")
		log.Debug()
		mastRun := &unstructured.Unstructured{
			Object: map[string]interface{}{
				"apiVersion": mastRunGVR.GroupVersion().String(),
				"kind":       "MastRun",
				"metadata": map[string]interface{}{
					"name": args[0] + "-" + RandString(5),
				},
				"spec": map[string]interface{}{
					"mastJobRef": map[string]interface{}{
						"name": mastjob.GetName(),
					},
				},
			},
		}
		result, err := dynamicClient.Resource(mastRunGVR).Namespace("mast").Create(context.TODO(), mastRun, metav1.CreateOptions{})
		if err != nil {
			log.Fatal(err)
		}
		log.Info("MastRun created, job ", result.GetName(), " is starting\n")

	},
}
