package cmd

import (
	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(jobCmd)
}

var jobCmd = &cobra.Command{
	Use:   "job",
	Short: "Perform action on mastjobs objects",
}
