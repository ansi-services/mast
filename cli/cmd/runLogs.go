package cmd

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"os"
	"path/filepath"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
)

var follow bool

func init() {
	runLogsCmd.Flags().BoolVarP(&follow, "follow", "f", false, "Specify if the logs should be streamed")
	runCmd.AddCommand(runLogsCmd)

}

var runLogsCmd = &cobra.Command{
	Use:   "logs",
	Short: "get logs from mastrun",
	Long:  `get logs from mastrun by providing mastrun name`,
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		if Verbose {
			log.SetLevel(log.DebugLevel)
		}

		dynamicClient := connectToK8s()

		mastrun, err := dynamicClient.Resource(mastRunGVR).Namespace("mast").Get(context.TODO(), args[0], metav1.GetOptions{})
		if err != nil {
			log.Fatal(err)
		}
		workflowName, isWorkFlow, err := unstructured.NestedString(mastrun.Object, "status", "mastrun_handler", "workFlow")
		if err != nil {
			log.Fatal(err)
		} else if isWorkFlow == false {
			log.Fatal("Mast run is a template or mast server is not working")
		} else if workflowName == "None" {
			handlerMessage, isHandlerMessage, err := unstructured.NestedString(mastrun.Object, "status", "mastrun_handler", "message")
			if err != nil || isHandlerMessage == false {
				log.Fatal(err)
			}
			fmt.Println(handlerMessage)
			return
		}
		workflow, err := dynamicClient.Resource(workflowGVR).Namespace("mast").Get(context.TODO(), workflowName, metav1.GetOptions{})
		if err != nil {
			log.Fatal(err)
		}

		nodes, isNodes, err := unstructured.NestedMap(workflow.Object, "status", "nodes")
		if err != nil {
			log.Fatal(err)
		} else if isNodes == false {
			log.Fatal("no nodes")
		}
		var ansibleContainer string
		for key, val := range nodes {
			if val.(map[string]interface{})["displayName"].(string) == "ansible-play" {
				ansibleContainer = key
			}
		}
		if ansibleContainer == "" {
			log.Fatal("Logs are nof available yet")
		}
		//get kubeconfig
		kubeconfig := filepath.Join(
			os.Getenv("HOME"), ".kube", "config")
		log.Debug("Using kubeconfig file: " + kubeconfig)
		config, err := clientcmd.BuildConfigFromFlags("", kubeconfig)
		if err != nil {
			log.Fatal(err)
		}

		// creates the clientset
		clientSet, err := kubernetes.NewForConfig(config)
		if err != nil {
			log.Fatal(err)
		}

		podLogOptions := v1.PodLogOptions{
			Container: "main",
			Follow:    follow,
		}

		podLogRequest := clientSet.CoreV1().Pods("mast").GetLogs(ansibleContainer, &podLogOptions)
		stream, err := podLogRequest.Stream(context.TODO())
		if err != nil {
			log.Info(err)
		}
		defer stream.Close()

		buf := new(bytes.Buffer)
		_, err = io.Copy(buf, stream)
		if err != nil {
			log.Fatal("error in copy information from podLogs to buf")
		}
		str := buf.String()
		println(str)

		os.Exit(0)
	},
}
