package cmd

import "k8s.io/apimachinery/pkg/runtime/schema"

var defaultNamespace = "mast"

var mastJobGVR = schema.GroupVersionResource{
	Group:    "mast.ansi.services",
	Version:  "v1",
	Resource: "mastjobs",
}

var mastRunGVR = schema.GroupVersionResource{
	Group:    "mast.ansi.services",
	Version:  "v1",
	Resource: "mastruns",
}

var mastOperatorGVR = schema.GroupVersionResource{
	Group:    "mast.ansi.services",
	Version:  "v1",
	Resource: "mastoperators"}

var workflowGVR = schema.GroupVersionResource{
	Group:    "argoproj.io",
	Version:  "v1alpha1",
	Resource: "workflows",
}
