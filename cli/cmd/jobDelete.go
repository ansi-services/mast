package cmd

import (
	"context"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func init() {
	jobCmd.AddCommand(jobDeleteCmd)
	log.SetLevel(log.InfoLevel)
}

var jobDeleteCmd = &cobra.Command{
	Use:   "delete [job name or file]",
	Short: "Delete mastjob",
	Long:  `Delete mastjob providing the name of the job or file`,
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		if Verbose {
			log.SetLevel(log.DebugLevel)
		}
		dynamicClient := connectToK8s()

		deletePolicy := metav1.DeletePropagationForeground
		deleteOptions := metav1.DeleteOptions{
			PropagationPolicy: &deletePolicy,
		}

		// if arg is a file
		if strings.Contains(args[0], ".yaml") || strings.Contains(args[0], ".yml") {
			file := getObjectDef(args[0])

			//convert yaml to map
			job := convertFileToMap(file)

			// get resource name
			var name = job["metadata"].(map[string]interface{})["name"].(string)
			log.Debug("Found job with name: " + name)
			//delete object
			if err := dynamicClient.Resource(mastJobGVR).Namespace("mast").Delete(context.TODO(), name, deleteOptions); err != nil {
				log.Fatal(err)
			}
		} else {
			if err := dynamicClient.Resource(mastJobGVR).Namespace("mast").Delete(context.TODO(), args[0], deleteOptions); err != nil {
				log.Fatal(err)
			}

		}
		log.Info("MastJob ", args[0], " deleted")
	},
}
