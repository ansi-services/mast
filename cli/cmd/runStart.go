package cmd

import (
	"context"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
)

func init() {
	runCmd.AddCommand(runStartCmd)
	log.SetLevel(log.InfoLevel)
}

var runStartCmd = &cobra.Command{
	Use:   "start [run file or name]",
	Short: "Start new mast run",
	Long:  `Start run from file or already exisiting run template`,
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		if Verbose {
			log.SetLevel(log.DebugLevel)
		}
		dynamicClient := connectToK8s()

		if strings.Contains(args[0], ".yaml") || strings.Contains(args[0], ".yml") {
			file := getObjectDef(args[0])

			run := convertFileToMap(file)

			runObject := &unstructured.Unstructured{run}
			//post new object to cluster
			result, err := dynamicClient.Resource(mastRunGVR).Namespace("mast").Create(context.TODO(), runObject, metav1.CreateOptions{})
			if err != nil {
				log.Fatal(err)
			}
			log.Info("MastRun " + result.GetName() + " created\n")
		} else {
			mastrun, err := dynamicClient.Resource(mastRunGVR).Namespace("mast").Get(context.TODO(), args[0], metav1.GetOptions{})
			if err != nil {
				log.Fatal(err)
			}
			log.Debug("Found mastrun " + args[0])
			template, isTemplate, err := unstructured.NestedString(mastrun.Object, "spec", "template")
			if err != nil {
				log.Fatal(err)
			} else if isTemplate == false || template == "false" {
				log.Fatal("mastrun " + args[0] + " is not in a template")
			}
			err2 := unstructured.SetNestedField(mastrun.Object, "false", "spec", "template")
			if err2 != nil {
				log.Fatal(err2)
			}
			log.Debug("Set mastrun template property to false")
			mastrun.SetName(mastrun.GetName() + "-" + RandString(5))
			result, err := dynamicClient.Resource(mastRunGVR).Namespace("mast").Create(context.TODO(), mastrun, metav1.CreateOptions{})
			if err != nil {
				log.Fatal(err)
			}
			log.Info("MastRun ", result.GetName(), " created\n")
		}
	},
}
