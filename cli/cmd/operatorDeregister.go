package cmd

import (
	"context"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func init() {
	operatorCmd.AddCommand(operatorDeregisterCmd)
	log.SetLevel(log.InfoLevel)
}

var operatorDeregisterCmd = &cobra.Command{
	Use:   "deregister [operator name or file]",
	Short: "Deregister operator",
	Long:  `Deregister operator providing the name of the operator or file`,
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		if Verbose {
			log.SetLevel(log.DebugLevel)
		}
		dynamicClient := connectToK8s()

		deletePolicy := metav1.DeletePropagationForeground
		deleteOptions := metav1.DeleteOptions{
			PropagationPolicy: &deletePolicy,
		}

		// if arg is a file
		if strings.Contains(args[0], ".yaml") || strings.Contains(args[0], ".yml") {
			file := getObjectDef(args[0])

			//convert yaml to map
			operator := convertFileToMap(file)

			// get resource name
			var name = operator["metadata"].(map[string]interface{})["name"].(string)
			log.Debug("Found Operator with name: " + name)
			//delete object
			if err := dynamicClient.Resource(mastOperatorGVR).Namespace("mast").Delete(context.TODO(), name, deleteOptions); err != nil {
				log.Fatal(err)
			}

		} else {
			if err := dynamicClient.Resource(mastOperatorGVR).Namespace("mast").Delete(context.TODO(), args[0], deleteOptions); err != nil {
				log.Fatal(err)
			}
		}
		log.Info("MastOperator ", args[0], " deregistered")

	},
}
