package cmd

import (
	"context"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
)

func init() {
	operatorCmd.AddCommand(operatorRegisterCmd)
	log.SetLevel(log.InfoLevel)
}

var operatorRegisterCmd = &cobra.Command{
	Use:   "register [operator file]",
	Short: "Register new operator",
	Long:  "Register new operator in the cluster, subtmiting name of yaml file with definition",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		if Verbose {
			log.SetLevel(log.DebugLevel)
		}

		file := getObjectDef(args[0])

		operator := convertFileToMap(file)

		dynamicClient := connectToK8s()

		//convert yaml to json
		operatorObject := &unstructured.Unstructured{operator}
		//post new object to cluster
		result, err := dynamicClient.Resource(mastOperatorGVR).Namespace("mast").Create(context.TODO(), operatorObject, metav1.CreateOptions{})
		if err != nil {
			log.Fatal(err)
		}
		log.Info("MastOperator ", result.GetName(), " registered\n")
	},
}
