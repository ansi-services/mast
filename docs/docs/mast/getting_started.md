# Getting Started

If you would like to give mast a fast try, here is how to install it and run a famous "Hello World".

To do this, you need to Kubernetes cluster and kubectl CLI, in order to set up and access the cluster.

- [kind](https://kind.sigs.k8s.io/)
- [minikube](https://minikube.sigs.k8s.io/docs/)
- Docker Desktop
- k3s
- k3d or any full-fledged cluster

### Install Mast server

Head over to the [releases page](https://gitlab.com/ansi-services/mast/-/releases) and install server and cli. The latest release labeled _Stable_ is recommended.

The command to install server looks something like that; just remember to replace **version** with your desired version number

```bash
kubectl apply -f https://gitlab.com/api/v4/projects/21527026/packages/generic/mast/<VERSION>/mast-installer.yaml
```

??? example "output"


Wait until all pods are up and running.

### Install CLI

Cli for diferent OS types and architectures can be find on [releases page](https://gitlab.com/ansi-services/mast/-/releases), provided with instalation instructions as well.

## Add your firsr MastJob and run it

Submit your firts MastJob:

```bash
mast job create https://gitlab.com/ansi-services/mast/-/raw/master/examples/hello-world/hello-world-mastjob.yaml
```

??? example "output"
    ```bash 
    INFO[0000] MastJob hello-world created 
    ```

You can do it also with standard `kubectl apply -f` command. To run any mastjob, asnible playbook is needed so mast would know what actions perform.

Simply add it by:

```bash
kubectl apply -f https://gitlab.com/ansi-services/mast/-/raw/master/examples/hello-world/hello-world-playbook.yaml
```

MastJob is just a definition of action or set of actions to do, to start MastJob you need a MastRun object, but the manifest of MastJob which you applied earlier needs confiMap with ansible playbook. Lets create it:

```bash
kubectl apply -f https://gitlab.com/ansi-services/mast/-/raw/master/examples/hello-world/hello-world-playbook.yaml
```

MastJob can be created as a code, defined in .yaml file or dynamicly with Mast CLI.

For now use CLI:

```bash
mast job run hello-world
```

??? example "output"
    ```bash 
    INFO[0000] MastRun created, job hello-world-xxxxx is starting 
    ```

This will generate MastRun and add it to the cluster.

You can list all MastRuns with:

```bash
mast run list
```

??? example "output"
    ```bash 
    NAME STATUS AGE hello-world-xxxxx Template 14m 
    ```

You can see that MastRun name is `hello-world-` ad a 5 random characters. This is done to diferenciate Mastruns who run the same Mastjob for few times.

Finally to see that promissed "hello world" run:

```bash
mast run logs <MastJob_name>
```

??? example "output"

    ```bash
    [WARNING]: No inventory was parsed, only implicit localhost is available [WARNING]: provided hosts list is empty, only localhost is available. Note that the implicit localhost does not match 'all'

      PLAY [localhost] ***************************************************************

      TASK [Gathering Facts] *********************************************************
      ok: [localhost]

      TASK [Hello from ansible inside your cluster] **********************************
      ok: [localhost] => {
          "msg": "Hello there"
      }

      PLAY RECAP *********************************************************************
      localhost                  : ok=2    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
    ```

Just add right MastJob name at the end of command.
````
