# What is Mast?

Mast is small and lightweight ansible runner for Kubernetes. With minimal requirement it can be used everywhere even it is your local minikube or big production cluster.

- run ansible in kubernetes cluster to perform automations inside and outside the cluster
- easily create operators to perform actions on objects and its events
- run repetetive configuration jobs defined as code in your kubernetes environemnt
