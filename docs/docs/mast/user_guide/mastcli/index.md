## Mast CLI

You can manage and get the basic info of the Mast resources with Mast CLI

Mast CLI is resource oriented. This means that mast command look like this:

```
mast <rescource type> <action> <resource name>
```

for example to list MastJobs present in your current cluster\`s namespace you would run

``` 
mast job list
```

As you can see in example above there is no *resource name*, this the only action when you do not pass *resource name*. List command returns list of all objects present in current cluster\`s namespace, passing resource name would have no use here.

To check all command and flags run   

```
mast --help
```

## Kubectl

Since all Mast resources are Kubernetes CRDs you can manage them with `kubectl` , but with mast cli it is a little easier.
          



