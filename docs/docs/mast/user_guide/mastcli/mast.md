## mast

Mast is a tool for creating and running ansible jobs inside kubernetes cluster

### Synopsis

Mast is a tool for simplifying jobs and tasks performed on kubernetes. This CLI allows to manage all mast resources such as jobs, runs and operators to perform various actions, from provisioning object in cluster to running as automation system

### Options

```
  -h, --help      help for mast
  -v, --verbose   verbose output
      --version   version for mast
```

### SEE ALSO

* [mast completion](mast_completion.md)	 - Generate the autocompletion script for the specified shell
* [mast job](mast_job.md)	 - Perform action on mastjobs objects
* [mast operator](mast_operator.md)	 - Perform operations on operators
* [mast run](mast_run.md)	 - Manage run objects

###### Auto generated by spf13/cobra on 20-Sep-2022
