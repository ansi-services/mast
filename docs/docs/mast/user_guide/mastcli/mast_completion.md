## mast completion

Generate the autocompletion script for the specified shell

### Synopsis

Generate the autocompletion script for mast for the specified shell.
See each sub-command's help for details on how to use the generated script.


### Options

```
  -h, --help   help for completion
```

### Options inherited from parent commands

```
  -v, --verbose   verbose output
```

### SEE ALSO

* [mast](mast.md)	 - Mast is a tool for creating and running ansible jobs inside kubernetes cluster
* [mast completion bash](mast_completion_bash.md)	 - Generate the autocompletion script for bash
* [mast completion fish](mast_completion_fish.md)	 - Generate the autocompletion script for fish
* [mast completion powershell](mast_completion_powershell.md)	 - Generate the autocompletion script for powershell
* [mast completion zsh](mast_completion_zsh.md)	 - Generate the autocompletion script for zsh

###### Auto generated by spf13/cobra on 20-Sep-2022
