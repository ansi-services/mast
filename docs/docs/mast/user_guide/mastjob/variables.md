#Variables

In **vars** section you can define variables to customize Mastjob to diferent scenarios.

There are 4 ways to define variables. The first two are the same as in playbook and inventory sections. So `fromConfigMap` and `fromRepo`. The last two is somehow new, there are `fromExtRepo` and `fromLiteral`.

Order is important in `vars` section if variables repeats itself in next vars source definition, the variables will be overwritten, and so on and so on. This can be helpful but keep this in mind when definig vars.

## Variables from ConfigMap

You can submit variables file as ConfigMap and then mention it name and key under which is your file.

```yaml
spec:
  ...
  configuration:
    playbook:
        ...
    vars:
      - fromConfigMap:
          name: vars-config
          key: vars.yaml
```

## from repository

`fromRepo` means that you can point to variables file from your repository mentioned in the `repository` section. Path mast be path in your repo where your desired file is located

```yaml
spec:
  ...
  configuration:
  	playbook:
      ...
    variables:
      - fromRepo:
        path: ansible-hello/variables.yaml
```

# From external repo

You can set a new reposistory source for variables section and get some file from another repository. This repository is defined in the same way as the main one. You can check it [here](variables.md/#from-repository).

The one new thing is path to precise where the file is exactly.

```yaml
spec:
  ...
  configuration:
  	playbook:
      ...
    variables:
      - fromExtRepo:
        git:
     			url: https://gitlab.com/mast.git
    			revision: master
    			workDir: path/to/my/folder
					path: path/to/file
```

## From literal

From literal means that you can write your variables in Mastjob. Simply define the name and its value in key-value format. Variables will be translated and added to all variables for ansible.

```yaml
spec:
	configuration:
		playbook:
			...
  	vars:
      - fromLiteral:
        - my_extra_var: "i am from literal"
```
