# Inventories

Inventory section is an array of inventoy files. Inventory file should have names, or adresess of machines and services which you want to configure or perform some actions. If you define a few inventories they will be passed as few files Ansible.

## Inventory from ConfigMap

You can submit the inventory as ConfigMap and then mention it name and key under which is your inventory.

```yaml title="inventory_configmap.yaml"
apiVersion: v1
kind: ConfigMap
metadata:
    name: inventory-config
    namespace: mast
data:
    inventory: |
        [test]
        mymachine2 ansible_connection=local
```

```yaml title="mastjob.yaml"
spec:
  ...
  configuration:
    playbook:
        ...
    inventory:
      - fromConfigMap:
          name: inventory-config
          key: inventory
```

## from repository

`fromRepo` means that you can point to inventory from your repository mentioned in the `repository` section. `path` is the path in your repo where your desired file is located.

```yaml
spec:
   ...
   configuration:
    playbook:
        ...
    inventory:
      - fromRepo:
          path: ansible-hello/inventory
```
