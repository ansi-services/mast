# MastJob

MastJob is a definition of job to be performed by MastRun.
The most important is `spec` section. It contains few fields to customize MastJob to your needs


```
--8<-- "./../controller/tests/test_manifests/mj-git-basic-auth.yaml"
```

## Ansible version

You can set a desired version of Ansible which will be used in the second step of MastJob. For now we have only 2.9.18  version, more version is on the way.

If you do not set any version, `latest` version is set by default in the moment of submitting MastJob to cluster.

```yaml
---
spec:
  ansible:
    version: your.desired.version
```

## Ansible Custom Image

If you wish to use your own image, you can do so by passing a URL to image registry.

```yaml
---
spec:
  ansible:
    image: your.image:latest
```

## Service account

You can set a service account which will be used to run pods with steps of your MastJob.

By default, this is `mast` service account

```yaml
---
spec:
  serviceAccount: your-sa
```

## Workflow name

By setting `WorkflowName` you can set the name of Argo workflow. This is the K8s resource to which MastJob triggered by MastRun is translated by Mast Server.

5 random digits are added at the end of this `WorkflowName` to avoid name collision.

```yaml
spec:
  workflowName: my-workflow
```

If `workflowName` is not set, the name of the workflow will be constructed from the following schema will be the name of MastRun which run this MastJob plus 4 random numbers

## Git repository

You can set git repository as a source of data for your MastJob.

```yaml
repository:
  git:
    url: https://gitlab.com/mast.git
  revision: main # (1)
  workDir: path/to/my/folder # (2)
```

1. Revision can be a branch, tag or commit. Default is `main`.
2. `workDir` is a path in your repo from where you want to copy files. If not set` the whole repository is being copied.

When you define a `repository`, files from your repo will be downloaded in _gather_dependencies_ step of workflow. Files are being moved folder in volume. This volume is then mounted in the next step of workflows which from Ansible is being run.

If your repo is private, you can provide credentials. For now, Mast supports only basic user auth with username and password or access token. Credentials have to be stored in Kubernetes Secret.

```yaml
repository:
  git:
    url: https://gitlab.com/mast.git
    credentials:
      basicAuth:
        usernameSecret:
          name: test-secret-basic-auth
          key: username
        passwordSecret:
          name: test-secret-basic-auth
          key: password
```
