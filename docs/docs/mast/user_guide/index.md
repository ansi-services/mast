# About

Mast is an automation tool, which utilize Ansible in Kubernetes environment. It is built on top [Argo Workflows](https://github.com/argoproj/argo-workflows/). Mast implements a few Kubernetes CRDs(Custom Resource Definition). 