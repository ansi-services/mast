# Create your first MastJob (Hello World)

This is the time when you should create your first MastJob. So yes, this is beloved 'Hello World'.

Before you get dirty with code, remember to install mast in your cluster and set your current namespace to **mast**

First things firsts, all actions on cluster at the very end are executed by Ansible. You need to create a playbook to print our 'Hello world' message.

The playbook looks like this:
```yaml title="playbook.yaml"
- hosts: localhost
    tasks:
        
        - name: Hello from ansible inside your cluster
            debug:
            msg: "Hello world"
```

To make this playbook possible to use in cluster, you have to add it as a ConfigMap. 

```yaml title="playbook-configmap.yaml"
apiVersion: v1
kind: ConfigMap
metadata:
    name: hello-world-playbook
data:
    playbook.yaml: |
        - hosts: localhost
          tasks:

            - name: Hello from ansible inside your cluster
              debug:
                msg: "Hello there"
```

Now is the time to write your first MastJob.
```yaml title="hello-world-mastjob.yaml"
apiVersion: mast.ansi.services/v1
kind: MastJob
metadata:
  name: hello-world
  namespace: mast
spec:
  ansible:
    version: latest
  configuration: # (1)
    playbook:    # (2)
      - fromConfigMap:  # (3)
          name: hello-world-playbook
          key: playbook.yaml
```

1.  Configuration is where you can define inputs as playbooks,inventories and variables used inside ansible runner" .
2.  Playbook section is required, no ansible run without playbook after all. 
3.  There you specify where is playbook located, name is a name of Configmap and key is keyname of file in that ConfigMap.


Add your MastJob to cluster with `mast` CLI or `kubectl`.


The last step is to run this MastJob. You can do it with Mast CLI
```bash
mast job run hello-world
```
Or by submitting MastRun file with `kubectl`:

```yaml title="hello-world-mastrun.yaml"
apiVersion: mast.ansi.services/v1
kind: MastRun
metadata:
  name: helo-world
  namespace: mast
spec:
  mastJobRef:
    name: hello-world
```

Use `mast run list` to check wheather MastRun is being executed and `mast run logs` to see your message.
