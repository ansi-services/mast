# Core concepts

The basic element of Mast is MastJob. In MastJob you can define all playbooks, inventories and variables for Ansible to consume. MastJob by itself is just a definition. It is translated by Mast server, a small pod which performs all the actions on Mast objects.

Mast server translates MastJob definition to specified two-step Argo workflow. The first step is responsible for gathering all inputs files needed for Ansible to perform the playbook. Second step is the Ansible runner itself, doing all the instructions defined in playbook(s)

But Mast server does not know when to run MastJob. User has to submit MastRun, so Mast server would know that this is the time to run certain MastJob. MastRun can be created dynamically with `mast job run` command or as code by a Kubernetes object definition in `.yaml` file.

In MastRun definition, you can also override configuration specified in MastJob. For example, you can have a MastJob to configure Postgres database, but you would like to configure it in staging and production environments. You can have one MastJob, just run it with different variables. More you about overrides you can read [here](mastrun/index.md#overriding).

The last but not least is MastOperator object, which allows you to create a lightweight, easy to configure Kubernetes Operators. All about it, is written [here](operator/index.md).
