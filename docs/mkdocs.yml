site_name: Mast
repo_url: https://gitlab.com/ansi-services/mast
repo_name: ansi-services/mast

nav:
  - mast/index.md
  - mast/quick_start.md
  - User Guide:
      - mast/user_guide/index.md
      - Core concepts: mast/user_guide/core_concepts.md
      - Hello World: mast/user_guide/hello_world.md
      - Mast CLI:
          - mast/user_guide/mastcli/index.md
          - command reference:
              - mast/user_guide/mastcli/mast.md
              - Mast job:
                  - mast/user_guide/mastcli/mast_job.md
                  - mast/user_guide/mastcli/mast_job_create.md
                  - mast/user_guide/mastcli/mast_job_delete.md
                  - mast/user_guide/mastcli/mast_job_list.md
                  - mast/user_guide/mastcli/mast_job_run.md
              - Mast run:
                  - mast/user_guide/mastcli/mast_run.md
                  - mast/user_guide/mastcli/mast_run_abort.md
                  - mast/user_guide/mastcli/mast_run_delete.md
                  - mast/user_guide/mastcli/mast_run_info.md
                  - mast/user_guide/mastcli/mast_run_list.md
                  - mast/user_guide/mastcli/mast_run_logs.md
                  - mast/user_guide/mastcli/mast_run_start.md
              - Mast Operator:
                  - mast/user_guide/mastcli/mast_operator.md
                  - mast/user_guide/mastcli/mast_operator_deregister.md
                  - mast/user_guide/mastcli/mast_operator_list.md
                  - mast/user_guide/mastcli/mast_operator_register.md
              - Completion:
                  - mast/user_guide/mastcli/mast_completion.md
                  - mast/user_guide/mastcli/mast_completion_bash.md
                  - mast/user_guide/mastcli/mast_completion_fish.md
                  - mast/user_guide/mastcli/mast_completion_powershell.md
                  - mast/user_guide/mastcli/mast_completion_zsh.md

      - MastJob:
          - mast/user_guide/mastjob/index.md
          - Playbooks: mast/user_guide/mastjob/playbook.md
          - Inventories: mast/user_guide/mastjob/inventory.md
          - Variables: mast/user_guide/mastjob/variables.md
      - MastRun:
          - mast/user_guide/mastrun/index.md
      - Operator:
          - mast/user_guide/operator/index.md
  - Roadmap: https://gitlab.com/ansi-services/mast/-/issues
  - Slack: https://join.slack.com/t/ansiservices/shared_invite/zt-1g9w9om87-wzwiLqSPtG9e2WZdUmOuGQ

theme:
  icon:
    repo: fontawesome/brands/gitlab
  name: material
  features:
    - toc.follow
    - content.code.annotate
    - navigation.tracking
    - navigation.indexes
    - navigation.tabs
    - content.code.copy

extra:
  analytics:
    provider: google
    property: G-FJWLDEK3DB
  homepage: https://docs.ansi.services/mast/
  consent:
    title: Cookie consent
    description: >-
      We use cookies to recognize your repeated visits and preferences, as well
      as to measure the effectiveness of our documentation and whether users
      find what they're searching for. With your consent, you're helping us to
      make our documentation better.

markdown_extensions:
  - pymdownx.highlight:
      anchor_linenums: true
  - pymdownx.inlinehilite
  - pymdownx.snippets
  - pymdownx.superfences
  - def_list
  - admonition
  - pymdownx.details
  - pymdownx.tasklist:
      custom_checkbox: true
